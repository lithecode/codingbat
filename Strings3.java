/**
 * Created with IntelliJ IDEA.
 * User: innoz-sree
 * Date: 10/3/13
 * Time: 12:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class Strings3 {

    /**
     * Given a string, count the number of words ending in 'y' or 'z' -- so the 'y' in "heavy" and the 'z' in "fez" count, but not the 'y' in "yellow" (not case sensitive). We'll say that a y or z is at the end of a word if there is not an alphabetic letter immediately following it. (Note: Character.isLetter(char) tests if a char is an alphabetic letter.)
     * <p/>
     * countYZ("fez day") → 2
     * countYZ("day fez") → 2
     * countYZ("day fyyyz") → 2
     */
    public int countYZ(String str) {
        int count = 0;
        str = str.toLowerCase() + " ";
        for (int i = 0; i < str.length() - 1; i++)
            if ((str.charAt(i) == 'y' || str.charAt(i) == 'z')
                    && !Character.isLetter(str.charAt(i + 1)))
                count++;
        return count;
    }

    /**
     * Given two strings, base and remove, return a version of the base string where all instances of the remove string have been removed (not case sensitive). You may assume that the remove string is length 1 or more. Remove only non-overlapping instances, so with "xxx" removing "xx" leaves "x".
     * <p/>
     * withoutString("Hello there", "llo") → "He there"
     * withoutString("Hello there", "e") → "Hllo thr"
     * withoutString("Hello there", "x") → "Hello there"
     */
    public String withoutString(String base, String remove) {
        if (base.length() < remove.length()) return base;
        return base.toLowerCase().substring(0, remove.length()).equals(remove.toLowerCase()) ? withoutString(base.substring(remove.length()), remove) : base.charAt(0) + withoutString(base.substring(1), remove);
    }

    /**
     * Given a string, return true if the number of appearances of "is" anywhere in the string is equal to the number of appearances of "not" anywhere in the string (case sensitive).
     * <p/>
     * equalIsNot("This is not") → false
     * equalIsNot("This is notnot") → true
     * equalIsNot("noisxxnotyynotxisi") → true
     */

    public boolean equalIsNot(String str) {
        int isCount = 0, notCount = 0;
        for (int i = 0; i <= str.length() - 2; i++) {
            if (str.substring(i, i + 2).equals("is")) {
                isCount = isCount + 1;
            }
            if (str.substring(i, i + 3).equals("not")) {
                notCount = notCount + 1;
            }
        }
        return (isCount == notCount);
    }

    /**
     * We'll say that a lowercase 'g' in a string is "happy" if there is another 'g' immediately to its left or right. Return true if all the g's in the given string are happy.
     * <p/>
     * gHappy("xxggxx") → true
     * gHappy("xxgxx") → false
     * gHappy("xxggyygxx") → false
     */

    public boolean gHappy(String str) {
        if (str.length() == 0) return true;
        if (str.length() < 3) return false;

        if ((str.charAt(0) == 'g' || str.charAt(2) == 'g') && str.charAt(1) == 'g') {
            return gHappy(str.substring(3));
        }
        return gHappy(str.substring(1));
    }

    /**
     * We'll say that a "triple" in a string is a char appearing three times in a row. Return the number of triples in the given string. The triples may overlap.
     * <p/>
     * countTriple("abcXXXabc") → 1
     * countTriple("xxxabyyyycd") → 3
     * countTriple("a") → 0
     */

    public static int countTriple(String str) {
        if (str.length() < 3) return 0;
        if (str.charAt(0) == str.charAt(1) && (str.charAt(1) == str.charAt(2))) {
            return 1 + countTriple(str.substring(1));
        }
        return countTriple(str.substring(1));
    }

    /**
     * Given a string, return the sum of the digits 0-9 that appear in the string, ignoring all other characters. Return 0 if there are no digits in the string. (Note: Character.isDigit(char) tests if a char is one of the chars '0', '1', .. '9'. Integer.parseInt(string) converts a string to an int.)
     * <p/>
     * sumDigits("aa1bc2d3") → 6
     * sumDigits("aa11b33") → 8
     * sumDigits("Chocolate") → 0
     */

    public static int sumDigits(String str) {
        if (str.length() == 0) return 0;
        if (Character.isDigit(str.charAt(0))) {
            return Integer.parseInt(str.charAt(0) + "") + sumDigits(str.substring(1));
        } else {
            return sumDigits(str.substring(1));
        }
    }



}

