/**
 * Created with IntelliJ IDEA.
 * User: innoz-sree
 * Date: 10/1/13
 * Time: 2:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class Strings2 {
    /**
     * Given a string, return a string where for every char in the original, there are two chars.
     * doubleChar("The") → "TThhee"
     * doubleChar("AAbb") → "AAAAbbbb"
     * doubleChar("Hi-There") → "HHii--TThheerree" *
     */

    public String doubleChar(String str) {
        if (str.length() == 0) return "";
        else {
            return "" + str.charAt(0) + str.charAt(0) + doubleChar(str.substring(1));
        }
    }

    /**
     * Return the number of times that the string "hi" appears anywhere in the given string.
     * countHi("abc hi ho") → 1
     * countHi("ABChi hi") → 2
     * countHi("hihi") → 2
     */

    public int countHi(String str) {
        if (str.length() <= 1) return 0;
        else {
            if (str.charAt(0) == 'h' && str.charAt(1) == 'i') {
                return 1 + countHi(str.substring(2));
            }
        }
        return countHi(str.substring(1));
    }

    /**
     * Return true if the string "cat" and "dog" appear the same number of times in the given string.
     * catDog("catdog") → true
     * catDog("catcat") → false
     * catDog("1cat1cadodog") → true
     */

    public boolean catDog(String str) {
        int cats = 0;
        int dogs = 0;
        for (int i = 0; i < str.length() - 2; i++) {
            if (str.substring(i, i + 3).equals("cat"))
                cats += 1;
            if (str.substring(i, i + 3).equals("dog"))
                dogs += 1;
        }
        return cats == dogs;
    }

    /**
     * Return the number of times that the string "code" appears anywhere in the given string, except we'll accept any letter for the 'd', so "cope" and "cooe" count.
     * countCode("aaacodebbb") → 1
     * countCode("codexxcode") → 2
     * countCode("cozexxcope") → 2 *
     */

    public int countCode(String str) {
        if (str.length() <= 3) return 0;
        if (str.substring(0, 4).equals("code") || (str.substring(0, 2).equals("co") && str.charAt(3) == 'e')) {
            return 1 + countCode(str.substring(4));
        }
        return countCode(str.substring(1));
    }

    /**
     * Given two strings, return true if either of the strings appears at the very end of the other string, ignoring upper/lower case differences (in other words, the computation should not be "case sensitive"). Note: str.toLowerCase() returns the lowercase version of a string.
     * <p/>
     * endOther("Hiabc", "abc") → true
     * endOther("AbC", "HiaBc") → true
     * endOther("abc", "abXabc") → true
     */

    public boolean endOther(String a, String b) {
        int startingIndex;
        a = a.toLowerCase();
        b = b.toLowerCase();
        if (a.length() > b.length()) {
            startingIndex = a.length() - b.length();
            if (a.substring(startingIndex, a.length()).equals(b))
                return true;
            else {
                return false;
            }

        } else {
            startingIndex = b.length() - a.length();
            if (b.substring(startingIndex, b.length()).equals(a)) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Return true if the given string contains an appearance of "xyz" where the xyz is not directly preceeded by a period (.). So "xxyz" counts but "x.xyz" does not.
     * <p/>
     * xyzThere("abcxyz") → true
     * xyzThere("abc.xyz") → false
     * xyzThere("xyz.abc") → true *
     */

    public boolean xyzThere(String str) {
        if (str.length() < 3) return false;
        if (str.length() == 3) {
            return str.substring(0, 3).equals("xyz") ? true : false;
        }
        if (str.charAt(0) == '.') {
            if (str.substring(1, 4).equals("xyz")) return xyzThere(str.substring(4));
            return xyzThere(str.substring(1));
        }
        if (str.substring(0, 3).equals("xyz")) return true;
        if (str.substring(1, 4).equals("xyz") && str.charAt(0) != '.') return true;
        return xyzThere(str.substring(1));
    }


    /**
     * Return true if the given string contains a "bob" string, but where the middle 'o' char can be any char.
     * <p/>
     * bobThere("abcbob") → true
     * bobThere("b9b") → true
     * bobThere("bac") → false *
     */

    public boolean bobThere(String str) {
        if (str.length() <= 2) return false;
        if (str.charAt(0) == 'b' && str.charAt(2) == 'b') {
            return true;
        }
        return bobThere(str.substring(1));
    }

    /**
     * Given two strings, A and B, create a bigger string made of the first char of A, the first char of B, the second char of A, the second char of B, and so on. Any leftover chars go at the end of the result.
     * <p/>
     * mixString("abc", "xyz") → "axbycz"
     * mixString("Hi", "There") → "HTihere"
     * mixString("xxxx", "There") → "xTxhxexre"*
     */

    public String mixString(String a, String b) {
        String str = "";
        int i = 0;
        for (i = 0; i < a.length() && i < b.length(); i++) {
            str = str + a.charAt(i) + b.charAt(i);
        }
        if (i < a.length()) {
            for (int j = i; j < a.length(); j++)
                str = str + a.charAt(j);
        }
        if (i < b.length()) {
            for (int j = i; j < b.length(); j++)
                str = str + b.charAt(j);
        }
        return str;
    }

    /**
     * Given a string and an int N, return a string made of N repetitions of the last N characters of the string. You may assume that N is between 0 and the length of the string, inclusive.
     * <p/>
     * repeatEnd("Hello", 3) → "llollollo"
     * repeatEnd("Hello", 2) → "lolo"
     * repeatEnd("Hello", 1) → "o"
     */

    public String repeatEnd(String str, int n) {
        String string = "";
        for (int i = str.length() - n; i < str.length(); i++) {
            string = string + str.substring(str.length() - n, str.length());
        }
        return string;
    }



}
